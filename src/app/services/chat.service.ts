import { Injectable } from '@angular/core';
import { Welcomes } from '../interfaces/chatBot';
import { MessageType } from '../interfaces/enums';
import { Message } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  chatID: string;
  botName = 'Artificial Jad';
  newChat = false;

  constructor() {}

  getBotName() {
    return this.botName;
  }

  getGreetingMessages() {
    const message1 = this.generateMessage(Welcomes.welcomeMessage1, this.botName, MessageType.message);
    const message2 = this.generateMessage(Welcomes.inputYourName, this.botName, MessageType.getName);
    const startMessages: Message[] = [message1, message2];
    return startMessages;
  }

  // Chat Functions
  generateMessage(sendingMessage: string, name: string, type: MessageType, bot: boolean = true) {
    const newMessage: Message =
      {
        message: sendingMessage.trim(),
        isBot: bot,
        senderName: name,
        timestamp: new Date().getTime(),
        messageType: type
      };
    return newMessage;
  }
}
