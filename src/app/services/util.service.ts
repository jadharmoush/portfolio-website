import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  // Swal
  showSwalInputMessage(swalTitle: string, swalText: string) {
    return new Promise((resolve) => {
      swal.fire({
        title: swalTitle,
        text: swalText,
        input: 'text',
        showCancelButton: true,
        backdrop: false,
      }).then((result) => {
        if (result.isConfirmed) {
          resolve(result.value);
        } else {
          resolve(null);
        }
      });
    });
  }

  showSwalSuccessMessage(swalTitle: string, swalText: string) {
    swal.fire({
      title: swalTitle,
      text: swalText,
      icon: 'success',
      backdrop: false,
    });
  }

  showSwalErrorMessage(swalTitle: string, swalText: string) {
    swal.fire({
      title: swalTitle,
      text: swalText,
      icon: 'error',
      backdrop: false,
    });
  }

  showSwalMessage(swalTitle: string, swalText: string) {
    swal.fire({
      title: swalTitle,
      text: swalText,
      backdrop: false,
    });
  }

  showSwalToast(message: string, error = false) {
    const swalToast = swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer);
        toast.addEventListener('mouseleave', swal.resumeTimer);
      }
    });
    swalToast.fire({
      icon: error ? 'error' : 'success',
      title: message
    });
  }

  // Validators
  emailValidation(email: string) {
    const emailfilter = /^[\w._-]+[+]?[\w._-]+@[\w.-]+\.[a-zA-Z]{2,6}$/;
    if (!(emailfilter.test(email))) {
      return false;
    } else {
      return true;
    }
  }

  parseName(name: string) {
    const firstLetter = name[0].toUpperCase();
    const restOfName = name.substr(1).toLowerCase();
    return firstLetter + restOfName;
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
}
