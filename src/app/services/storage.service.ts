import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  async setValue(keyName: string, valueValue: string) {
    await Storage.set({
      key: keyName,
      value: valueValue
    });
  }

  async setObject(keyName: string, valueValue: any) {
    await Storage.set({
      key: keyName,
      value: JSON.stringify(valueValue)
    });
  }

  async getValue(keyName: string) {
    const { value } = await Storage.get({ key: keyName });
    return value;
  }

  async getObject(keyName: string) {
    const { value } = await Storage.get({ key: keyName });
    if (value) {
      return JSON.parse(value);
    } else {
      return null;
    }
  }

  async removeKey(keyName: string) {
    await Storage.remove({ key: keyName });
  }

  async clear() {
    await Storage.clear();
  }
}
