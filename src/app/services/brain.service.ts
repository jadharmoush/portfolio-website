import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Message, VisitorInformation } from '../interfaces/interfaces';
import { HttpClient  } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class BrainService {
  visitorInfo: VisitorInformation = null;
  sessionId: string = null;
  firstTime = false;

  constructor(
    private storage: StorageService,
    private firestore: AngularFirestore,
    private platform: Platform,
    private http: HttpClient
  ) { }

  initialize() {
    this.storage.getObject('visitorInformation').then((value: VisitorInformation) => {
      if (value) {
        this.visitorInfo = value;
        if (value.messages.length === 0) {
          this.firstTime = true;
        }
        if (value.visitorName) {
          console.log('Welcome back ', value.visitorName);
        }
        this.storage.getValue('sessionID').then((id) => {
          this.sessionId = id;
          this.updateIPLocation();
        });
      } else {
        this.createVisitorInformation().then((visitor: VisitorInformation) => {
          this.visitorInfo = visitor;
          this.firstTime = true;
          this.createNewSession(visitor).then((id: string) => {
            this.sessionId = id;
            console.log(this.sessionId);
            this.storage.setValue('sessionID', id);
            this.updateIPLocation();
          });
        });
      }
    });
  }

  // Get Data
  getVisitorInformation() {
    return this.visitorInfo;
  }

  getFirstTimeValue() {
    return this.firstTime;
  }

  // Create First Time Variables
  createVisitorInformation() {
    return new Promise((resolve) => {
      const dateNumber = new Date().getTime();
      const visitor: VisitorInformation = {
        visitorName: null,
        visitorEmail: null,
        visitorGender: null,
        avatarNumber: 1,
        noFormalsMode: false,
        firstDateOfVisit: dateNumber,
        lastDateOfVisit: dateNumber,
        messages: [],
        platform: this.platform.platforms()
      };
      this.storage.setObject('visitorInformation', visitor).then(() => {
        resolve(visitor);
      });
    });
  }

  updateIPLocation() {
    const getIP = this.http.get('https://api.ipify.org/?format=json').toPromise();
    getIP.then((data: any) => {
      if (data.ip) {
        this.visitorInfo.ipLocation = String(data.ip);
        this.visitorInfo.platform = this.platform.platforms();
        this.updateAllSession(this.visitorInfo);
      }
    });
  }

  updateVisitorInformation(info: VisitorInformation) {
    return new Promise((resolve) => {
      const dateNumber = new Date().getTime();
      info.lastDateOfVisit = dateNumber;
      this.storage.setObject('visitorInformation', info).then(() => {
        resolve(true);
      });
    });
  }

  // Clear Information
  clearAllInfo() {
    this.storage.clear().then(() => {
      this.initialize();
    });
  }

  // Server Communication
  createNewSession(visitor: VisitorInformation) {
    console.log('Creating New Session');
    return new Promise((resolve, reject) => {
      this.firestore.collection('visitors').add(visitor).then((doc) => {
        resolve(doc.id);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  updateSessionMessages(newMessages: Message[]) {
    return new Promise((resolve, reject) => {
      const dateNumber = new Date().getTime();
      this.visitorInfo.messages = newMessages;
      this.storage.setObject('visitorInformation', this.visitorInfo).then(() => {
        this.firestore.collection('visitors').doc(this.sessionId).update({messages: newMessages, lastDateOfVisit: dateNumber}).then(() => {
          resolve(true);
          console.log('stats saved');
        }).catch(() => {
          reject(true);
        });
      });
    });
  }

  updateAllSession(visitorInformation: VisitorInformation) {
    return new Promise((resolve, reject) => {
      this.visitorInfo = visitorInformation;
      const dateNumber = new Date().getTime();
      this.visitorInfo = visitorInformation;
      this.visitorInfo.lastDateOfVisit = dateNumber;
      this.storage.setObject('visitorInformation', this.visitorInfo).then(() => {
        this.firestore.collection('visitors').doc(this.sessionId).update(visitorInformation).then(() => {
          resolve(true);
          console.log('stats saved');
        }).catch(() => {
          reject(true);
        });
      });
    });
  }
}
