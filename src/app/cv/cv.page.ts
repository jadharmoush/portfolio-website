import { Component, OnInit } from '@angular/core';
import {ActionSheetController} from '@ionic/angular';
import {UtilService} from "../services/util.service";

@Component({
  selector: 'app-cv',
  templateUrl: './cv.page.html',
  styleUrls: ['./cv.page.scss'],
})
export class CvPage implements OnInit {

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private util: UtilService
  ) { }

  ngOnInit() {
  }

  async presentDownloadActions() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Download Cv',
      buttons: [{
        text: 'Download as PDF',
        icon: 'share',
        handler: () => {
         this.downloadCvPdf();
        }
      }, {
        text: 'Download as Word',
        icon: 'caret-forward-circle',
        handler: () => {
          this.downloadWord();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  copyNumber() {
    navigator.clipboard.writeText('+971585842305').then(() => {
      this.util.showSwalToast('Number copied to clipboard');
    }).catch(e => console.error(e));
  }

  copyEmail() {
    navigator.clipboard.writeText('contact@jadharmoush.com').then(() => {
      this.util.showSwalToast('Email copied to clipboard');
    }).catch(e => console.error(e));
  }

  goToWebsite() {
    this.util.showSwalToast('You are on the website :)');
  }

  goToLinkedIn() {
    window.open('https://www.linkedin.com/in/jad-h-60541a114/');
  }

  private downloadCvPdf() {
    window.open('https://jadharmoush.com/assets/files/JadCV.pdf');
  }

  private downloadWord() {
    window.open('https://jadharmoush.com/assets/files/JadCV.docx');
  }
}
