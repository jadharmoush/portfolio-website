import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { CountUpModule } from 'ngx-countup';

import { FabStyledButtonComponent } from './fab-styled-button/fab-styled-button.component';
import { ContactMeLightComponent } from './contact-me-light/contact-me-light.component';
import { MyStatsComponent } from './my-stats/my-stats.component';
import { DubaiArtComponent } from './dubai-art/dubai-art.component';

@NgModule({
  declarations: [
    FabStyledButtonComponent,
    ContactMeLightComponent,
    MyStatsComponent,
    DubaiArtComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CountUpModule
  ],
  exports: [
    FabStyledButtonComponent,
    ContactMeLightComponent,
    MyStatsComponent,
    DubaiArtComponent
  ]
})

export class ComponentsModule {}

