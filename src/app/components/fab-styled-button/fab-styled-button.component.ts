import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-fab-styled-button',
  templateUrl: './fab-styled-button.component.html',
  styleUrls: ['./fab-styled-button.component.scss'],
})
export class FabStyledButtonComponent implements OnInit {
  @Input() buttonText = 'Learn more';
  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() buttonClicked = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  clickButton() {
    this.buttonClicked.emit(true);
  }

}
