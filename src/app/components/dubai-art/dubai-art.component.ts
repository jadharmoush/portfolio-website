import { Component, Input, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-dubai-art',
  templateUrl: './dubai-art.component.html',
  styleUrls: ['./dubai-art.component.scss'],
})
export class DubaiArtComponent implements OnInit {
  @Input() dayCycle = true;
  @Input() showText = true;
  @Input() height = 100;
  @Input() showBackButton = true;

  // Audio
  audioOn = false;

  constructor(
    private navCtrl: NavController
  ) { }

  goBack() {
    this.navCtrl.back();
  }

  ngOnInit() {
    const wrapper = document.querySelector('.art_holder');
    const burj = document.querySelector('.burj') as any;
    const burjArab = document.querySelector('.burjArab') as any;
    const bigBuilding1 = document.querySelector('.building1') as any;
    const bigBuilding2 = document.querySelector('.building2') as any;
    const horizontalBuilding = document.querySelector('.horizontalBuilding') as any;
    const background2 = document.querySelector('.background2') as any;

    wrapper.addEventListener('mousemove', (e: any) => {
      const pageX = e.clientX;
      const pageY = e.clientY;

      burj.style.transform = 'translateX(' + pageX / 100 + '%) translateY(' + pageY / 100 + '%)';
      burjArab.style.transform = 'translateX(' + pageX / 150 + '%) translateY(' + pageY / 250 + '%)';
      bigBuilding1.style.transform = 'translateX(' + pageX / 50 + '%) translateY(' + pageY / 70 + '%)';
      bigBuilding2.style.transform = 'translateX(' + pageX / 80 + '%) translateY(' + pageY / 90 + '%)';
      horizontalBuilding.style.transform = 'translateX(' + pageX / 40 + '%) translateY(' + pageY / 80 + '%)';
      background2.style.transform = 'translateX(' + pageX / 400 + '%)';
    });
  }

  changeAudioState() {
    const audio = document.querySelector('audio');
    if (audio.paused) {
      audio.volume = 0.5;
      audio.play();
      this.audioOn = true;
    } else {
      audio.pause();
      this.audioOn = false;
    }
  }
}
