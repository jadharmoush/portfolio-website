import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-me-light',
  templateUrl: './contact-me-light.component.html',
  styleUrls: ['./contact-me-light.component.scss'],
})
export class ContactMeLightComponent implements OnInit {
  light = false;
  constructor() { }

  ngOnInit() { }

  switchLight() {
    this.light = !this.light;
  }
}
