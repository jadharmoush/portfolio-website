import { MessageType } from './enums';

export interface Message {
    id?: string;
    senderName: string;
    message: string;
    isBot: boolean;
    timestamp: number;
    messageType: MessageType;
    extraObject?: any;
}

export interface VisitorInformation {
    id?: string;
    visitorName: string;
    visitorEmail: string;
    visitorGender: string;
    avatarNumber: number;
    noFormalsMode: boolean;
    firstDateOfVisit: number;
    lastDateOfVisit: number;
    messages: Message[];
    platform?: string[];
    ipLocation?: string;
}
