export enum IonicColor {
    danger = 'danger',
    secondary = 'secondary',
    warning = 'warning',
    medium = 'medium',
    light = 'light'
}

export enum MessageType {
    message = 0,
    getName,
    wantInputDetails,
    getEmail,
    getGender,
    leaveMessage
}
