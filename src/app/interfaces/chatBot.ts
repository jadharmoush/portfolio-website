/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable max-len */

export enum SavedReplies {
    thankYou = 'Thank you 🤗',
    letsStartInput = 'Perfect ! Let\'s start :',
    infoSaved = 'Your Info Are Saved. To restart everything, you can type "clear all"',
    inputMoreInfo = 'If You would like me  to contact you later, you need to answer some questions. \nType "yes" if you don\'t mind, and "no" if you wish to continue without providing any additional information 😊',
    beginningText = 'Let\'s start $$. If You wish to go to my CV, you can type "CV", or if you want to leave a message type "leave message". \nElse feel free to ask for songs, quotes or jokes.. I\'m learning more every day, so I\'m sorry if I don\'t understand something now 🤓. PS: You can now type "change font" to display my real handwriting in messages',
    emailNotValid = 'The Email You Provided is not valid, Please provide a valid email 🥺',
    genderM = 'Okay Mr. $$, Nice to meet you !',
    genderF = ' Okay Ms. $$, Nice to meet you !',
    genderA = 'Nice to meet you $$ !',
    leaveMessage = 'Okay, All Messages below will be sent to Jad, Type "finished message" when you finish',
    finishedMessage = 'Aightt, messages sent and I\'m back ! You miss me $$ ?',
    changeFontMine = 'Now displaying My handwriting, to change it again.. type "change font"',
    changeFont = 'I know, my handwriting is bad 😭, font is back to normal"'
}

export enum SavedQuestions {
    getEmail = 'What\'s Your Email ?',
    getGender = 'What\'s Your Gender ? \nType "M" for Male, "F" for Female and "A" for Other'
}

export enum Welcomes {
    welcomeMessage1 = 'Hello There, looks like you\'re visiting my website for the first time !',
    inputYourName = 'What\'s you\'re Name ?'
}

export const ChatJokes: string[] = [
    'Hear about the new restaurant called Karma? There’s no menu: You get what you deserve.',
    'A woman in labor suddenly shouted, “Shouldn’t! Wouldn’t! Couldn’t! Didn’t! Can’t!” “Don’t worry,” said the doc. “Those are just contractions.”',
    'Did you hear about the actor who fell through the floorboards? He was just going through a stage.',
    'Did you hear about the claustrophobic astronaut? He just needed a little space',
    'Why don’t scientists trust atoms? Because they make up everything.',
    'Where are average things manufactured? The satisfactory.'
];

export const Quotes: string[] = [
    'In a world full of Kim Kardashians, be a princess Diana',
    'A mind wants to forget, but a heart will always remember',
    'Don\'t sleep like you\'re rich, work like you\'re broke',
    'Shoes and people: if they hurt you, they are not you\'re size',
    'Never trust a friend that chills with the enemy',
    'If you want change, you have to invite chaos',
    'The less you talk, the more people think about you\'re words'
];

export const Music: string[] = [
    'CLIQUE - Riles',
    'DRAGON - Booba',
    'GLORIOUS - Macklemore',
    'GOOD OLD DAYS - Macklemore',
    'TALK IS CHEAP - Nick Murphy & Chet Faker',
    'FEEL - SAFE',
    'NIGHT RIDER - Masked Wolf'
];
