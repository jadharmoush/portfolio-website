/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable max-len */
import { IonicColor } from './enums';

export const SocialLinks = {
    linkedIn: 'https://www.linkedin.com/in/jad-h-60541a114/',
    instagram: 'https://www.instagram.com/jad.harmoush/',
    whatsappDubai: '+971 58 584 2305',
    whatsappFrance: '+33 7 56 90 14 00',
    email: 'contact@jadharmoush.com',
    vsco: 'https://vsco.co/jadharmoush'
};

export const AvatarLinks: string[] = [
    '../../../assets/images/chat-service/jay.webp',
    '../../../assets/images/chat-service/jayLaptop.webp',
    '../../../assets/images/chat-service/momo.webp'
];

export const DeepThoughts: {title: string; thought: string; color: IonicColor}[] = [
    {
        title: 'Hurt',
        thought: 'Because it happens when you least expect it coming . Because you get too weak after being so attached to that something or someone. Because you wrap your breath and soul around their existence until yours becomes faded ,blurred, determined by their fate and choices.Because you give all you have until the last drop of blood . You live upon expectations, opinions, comments. You wait for their approval, their acceptance of your whole being .',
        color: IonicColor.danger
    },
    {
        title: 'Reach A Point',
        thought: 'Because sometimes you get to a point where nothing really matters anymore . because wether they\'re leaving or staying , it doesn\'t change anything really . because you find out that they never were your happiness . and you\'re still sad even around them . because you get to a point where its not about them anymore . its about you . a point where theres no issue . you wanna go so deep into your thoughts and find the source of this ache cause its burning in you. maybe its too late to search for it but you still want to know why. and maybe , you\'re just too damaged to even know',
        color: IonicColor.medium
    },
    {
        title: 'Evil',
        thought: 'Evil is everywhere, and the biggest evil is often disguised as good, it has to make you trust it, get closer and closer while sucking ur energy, ur life and ur soul. We think that hitler was one of the biggest evil, however he is nothing compared to what is out there. The only difference that sets him apart is him having power. Give power to any evil person and he would be able to do worse than hitler. And the biggest problem is that evil doesn’t think its evil.',
        color: IonicColor.light
    },
];

export const LovePoems: {title: string; poem: string; color: IonicColor}[] = [
    {
        title: 'I Have It All',
        poem: 'I don\'t just want something . I already have it , all of it . Having you feels like the universe in my hands . I want all of you , even your deepest fears , your sorrow , your pain . All of you in my life and all of me in yours . Im here and i choose to stay everything . I believe in you , in us . That is how i fight the emptiness in me , because you fill it with passion . I finally got to a state where i know what i want . and i want you . And nothing ever feels as good as knowing what you want and working for it , knowing that you found that someone and that you want to keep them forever . Nothing feels like knowing you\'re not alone , knowing that someone will always be there . and nothings like us . You\'re mine and i want to show you and give you all i have . I want us not to wish for better because we know there isn\'t . I want us to be a reason enough , to be happy , to stay happy , a reason to motivate us to do , to be , to live , to stay',
        color: IonicColor.danger
    },
    {
        title: 'Meanings',
        poem: 'It’s all about how his eyes glance at me while I\'m not aware. It\'s all about how his finger prints draw maps on my skin, my heart beat next to his. It\'s all about the way he laughs, he smiles, he breathes heavily while sleeping. It\'s his voice to what I wish I can wake up everyday. I can\'t not think about mu body wrapped in bis arms; our bodies covered in bed. It\'s all about how he is mine and I am his. And I couldn\'t ever be anything more perfect than being his. His very own possession. It\'s about having him and not wishing for more. It\'s about a state of stability, of satisfaction, of filling a void, a heart that was empty once. I am full. Full of emotions, I can\'t explain. Full of feelings all for you. I am complete, today and every single day since i met you. I don\'t care what you think, but I am never going to leave you. Even when I\'m leaving, I am not. It\'s about the long nights we spent together. It\'s about the magic that\'s between our souls; the magic you give to my heart. Heaven is a place on earth with you. And I\'m staying. And I love you, And I\'m grateful. ',
        color: IonicColor.light
    },
    {
        title: 'The Universe',
        poem: 'He looked at her eyes while seeing the universe, she was his doctor, his future, she was the perfect him. he was broken, and all she wanted was to repair him.. they had their own universe where love is taken to the top, love is more than anything, it can elevate him from dust to the sun and that\'s the part where he sees her as the sun and imagine you looking at the sun, so she was too good for him but he did not think abt it. however in his deepest problems he decided to face them alone and it was where he stopped existing, his love was gone and she was broken, so broken he could not repair what he broke so what he decided to do was the worst.. he decides to take his heart off, cold and full of burning blood, and all that remains was ashes and remains of a once perfect guys that\'s the moment where he became the monster he was always afraid of becoming chasing his loved ones, and taking more ashes into his breath, he was full of it and there was no turning back, it is done he said and dead on her feet he fell.. his last breath was: it was all about me not having money, she looked at him saying money is nothing, your my everything but he was not convinced, he looked at her eyes and said its not about money, its about freedom and then she looked at him with her eyes where the shine has gone deep with cold tears. i love you and i will always do yet you choose to end our novel instead of never stopping however my only wish now is that there\'s another life so i can be with you in it. bye',
        color: IonicColor.warning
    },
    {
        title: 'Worth Dying For',
        poem: 'when i saw you, you were the sun & now i have the sun for me.. i\'m the luckiest person in the universe, you are my shining light in the darkness, my caring heart in my hard times.. your smile can fix me and your heart can elevate me so high i forget reality & you, you are my everything. i wanna take care of you, now & forever. your worth dying for 🌗 \nI\'d never thought that i would blindly fall in love but i did and surprisingly i never regretted it, cz i fell in love with my other half, with my happiness, with my God. And this is more than anyone hopes to get. I will be there to write our novels together in our own world cz when home became you, i knew that me too, i would be more than fully committed, i would be all yours and i am all yours, you can break me and heal me now. And I am the luckiest person to have you taking care of me, and looking out for me, i would die just to make you smile, to take care of you and make you live the happiest life ever. I want you, only you.',
        color: IonicColor.secondary
    },
    {
        title: 'Weird Feelings',
        poem: 'I\'d never thought that i would blindly fall in love but i did and surprisingly i never regretted it, cz i fell in love with my other half, with my happiness, with my God. And this is more than anyone hopes to get. I will be there to write our novels together in our own world cz when home became you, i knew that me too, i would be more than fully committed, i would be all yours and i am all yours, you can break me and heal me now. And I am the luckiest person to have you taking care of me, and looking out for me, i would die just to make you smile, to take care of you and make you live the happiest life ever.I want you, only you.',
        color: IonicColor.medium
    },
    {
        title: 'Long Time',
        poem: 'I’m writing for you, for its been a long time since. And i wanna say that i\'m sorry, I\'m deeply sorry for every extra heart beat i made you have, for every tear that poured from your beautiful eyes. I made you question our love instead of making it an absolute truth that the greatest philosophers can\'t deny. However you stood there, never looked back, never left me for a second. You stood there and gave me love in times where i didn\'t deserve it. I now know that we will have each other for every happiness and misery we face and that\'s what keeps me going, keeps me happy that i\'m alive, that i breath to see you another day, touch your body, your hair and sleep on your chest. I wanna experience life with you, take care of you, be there for you. For this is my reason to live now, until someone dies in the hand of the other.',
        color: IonicColor.danger
    },
    {
        title: 'Rules',
        poem: 'Life is fucked up, we\'re following rules we don\'t know and social behaviors we didn\'t make and waiting for our eventual end.However you, you made me see the meaning of life, the power that makes me wake up everyday smiling. Why? Because your smile makes my life and your tears awakes the predator in me that is protecting you from any harm. It is called love what\'s between us, but I say it\'s more than that, Its more than the universe will ever understand and its because we\'re different, we\'re special & we\'re pure. All I can see in this world is you and for all the happiness that exists in it, it is nothing without you. I love you, the pure you, the naked you, the funny you, the sad you and the angry you. You are the sun and I am the moon, you shine on me and I shine on you... ',
        color: IonicColor.light
    },
    {
        title: 'She Told Me',
        poem: 'You told that you miss my poems, but we are the poems, its what makes our love special and our bond unbreakable, its what makes ur smile bigger than the whole world bringing joy all over my life giving me another smile that redo what you did to me in an everlasting way. And what is poetry without the bad moments, the moments of struggle where we really prove our bond and test its strength, where we test our hearts and our tears, as these are essential in a poem u know, because without the paradox of love, the struggle, the sadness, we would never appreciate the good moments and nether to say that the bad moments are what teaches us to be happier and solidifies our bonds. We will face the bad and the bad will face us but whatever happens we will never let our bond break, and its steal to shatter into pieces because its peaces will pierce right into our heart and eyes making us bleed deeply from the inside, where we won\'t be able to continue our lives the way it was meant to be. Love me like i love you and let us conquer the whole world together.',
        color: IonicColor.secondary
    },
    {
        title: 'Things Left Unsaid',
        poem: 'No matter how long we know each other ; ill always have something to tell you , things unsaid , left behind or things I want you to know . you never left , not a second , from my mind . you\'ve been haunting my thoughts ever since , controlling my life , ruling my world . you\'re the king of my empire , a powerful one cause even your words have power on me . it takes One simple word from you to make my day the brightest or the darkest ever . You have the power to change everything , your impact on me is indescribable , like magic . You turn my world into a fairy\'s one , my nightmares into daydreams , my fears into affection . this power doesn\'t acquire your presence near or your love , it happens naturally even when you\'re hundred miles away . I cant describe it more than that even though its so much bigger and better . The distance between us doesn\'t change anything ; and if there wasn\'t any , im afraid i\'ll be so much more melting in your love that i\'ll forget my existence on earth , im afraid love would be so strong and hard on me that it will lead me to my end , im afraid ill worship you like no one has ever worshiped a god , that ill be in love with every breath of you , every smile , every word you say , every move . The touch of you takes me far away, the taste of your lips makes me live paradise , my own kind of paradise that is beside you. Let the distance keep us together',
        color: IonicColor.warning
    },
    {
        title: 'My Favorite Place',
        poem: 'Your arms are my favorite place to be. I already miss you, miss your hair, miss your touch. Its amazing how distance couldn\'t take you away from me, how our souls have been closer than ever. I never thought about leaving you, leaving us. I would never. It feels so different now; after all these years to have finally found security. You\'re my home. I can\'t not laugh about all my past relationships with people, all kinds of relationships that broke my heart and took me a while to recover. What a loss of time! All i ever wished was to have found you sooner. But i can\'t guarantee going back in time. I can only promise you the future, all my future with you. I want to spend every single moment of every single day next to your heartbeat. I want to hold you so so tight and feel the universe in my hands. You are my universe. I want to wake up and sleep to the sound of your voice echoing in my mind, haunting my life. I want you yo have all of me, and i have all of you. I accept and want and would die to live with you and your flaws and your imperfections. I want you every day for the rest of my life. I can\'t believe what i\'m saying but i know that i want to marry you.',
        color: IonicColor.light
    },
    {
        title: 'Terrifying',
        poem: 'Your arms are my favorite place to be. I already miss you, miss your hair, miss your touch. Its amazing how distance couldn\'t take you away from me, how our souls have been closer than ever. I never thought about leaving you, leaving us. I would never. It feels so different now; after all these years to have finally found security. You\'re my home. I can\'t not laugh about all my past relationships with people, all kinds of relationships that broke my heart and took me a while to recover. What a loss of time! All i ever wished was to have found you sooner. But i can\'t guarantee going back in time. I can only promise you the future, all my future with you. I want to spend every single moment of every single day next to your heartbeat. I want to hold you so so tight and feel the universe in my hands. You are my universe. I want to wake up and sleep to the sound of your voice echoing in my mind, haunting my life. I want you yo have all of me, and i have all of you. I accept and want and would die to live with you and your flaws and your imperfections. I want you every day for the rest of my life. I can\'t believe what i\'m saying but i know that i want to marry you.',
        color: IonicColor.medium
    },
    {
        title: 'Fake Or Real ?',
        poem: 'I can tell you now why I love you so much. I’m ready. (It’s actually really hard for me to write this but anyway mdrr) You know, one day we’ll both get old. Time flies and goes by very fast, the hours will rush on and minutes won’t get any slower and when I think about this, about future, I think about you. That’s at this exact moment I knew I fell for you, I knew it was real. I never thought of falling in love but here I am completely crazy about you (you should have seen my face when I realized you forgot to tell me good night) Your laugh, your sleepy baby voice, your mind, your unconditional kindness towards me, made me fall in love with you, completely. You are everything I could ever need, I’m fully yours, my mind, my body, my hopes, my heart.. all this is reserved for you only, and this for eternity. Now, 10000km away I only wish to see you, I have no other craving than to have you in my arms, holding me tight like you used to all night. Telling me about your feelings and your worst fears. Or about your past and your hopes. My love, that is how I am now desperately waiting for us to meet again.',
        color: IonicColor.danger
    }
];

