import { Pipe, PipeTransform } from '@angular/core';
import * as dayjs from 'dayjs';
import * as calendar from 'dayjs/plugin/calendar';
dayjs.extend(calendar);

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (args[0] === 'transform') {
      const dateElement = value as number;
      const date = new Date(dateElement);
      return dayjs().calendar(dayjs(date), {
        sameDay: '[Today at] h:mm A', // The same day ( Today at 2:30 AM )
        nextDay: '[Tomorrow at] h:mm A', // The next day ( Tomorrow at 2:30 AM )
        nextWeek: 'dddd [at] h:mm A', // The next week ( Sunday at 2:30 AM )
        lastDay: '[Yesterday at] h:mm A', // The day before ( Yesterday at 2:30 AM )
        lastWeek: '[Last] dddd [at] h:mm A', // Last week ( Last Monday at 2:30 AM )
        sameElse: 'DD/MM/YYYY' // Everything else ( 17/10/2011 )
      });
      return null;
    } else {
      return value;
    }
  }

}
