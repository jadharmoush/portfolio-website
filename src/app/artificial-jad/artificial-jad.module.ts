import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArtificialJadPageRoutingModule } from './artificial-jad-routing.module';

import { ArtificialJadPage } from './artificial-jad.page';

import { PipesModule} from '../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ArtificialJadPageRoutingModule,
    PipesModule
  ],
  declarations: [ArtificialJadPage]
})
export class ArtificialJadPageModule {}
