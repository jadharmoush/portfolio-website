/* eslint-disable */
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {ModalController, NavController} from '@ionic/angular';
import { ChatJokes, Music, Quotes, SavedQuestions, SavedReplies, Welcomes } from 'src/app/interfaces/chatBot';
import { MessageType } from 'src/app/interfaces/enums';
import { Message, VisitorInformation } from 'src/app/interfaces/interfaces';
import { AvatarLinks } from 'src/app/interfaces/static';
import { BrainService } from 'src/app/services/brain.service';
import { ChatService } from 'src/app/services/chat.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-artificial-jad',
  templateUrl: './artificial-jad.page.html',
  styleUrls: ['./artificial-jad.page.scss'],
})
export class ArtificialJadPage implements OnInit {
  @ViewChild('content') private content: any;

  visitorInformation: VisitorInformation = null;
  // Inputs
  sendingMessage = new FormControl('', [Validators.required]);
  // Bot Helpers
  lastBotMessageType = MessageType.getName;
  botTyping = false;
  botImage = AvatarLinks[0];
  visitorImage = AvatarLinks[1];
  handwritingMode = false;

  constructor(
    private modalCtrl: ModalController,
    private chatService: ChatService,
    private util: UtilService,
    private brainService: BrainService,
    private navCtrl: NavController
  ) {
    this.initialize();
  }

  ngOnInit() {
    setTimeout(() => {  this.scrollToBottom(); }, 1500);
  }

  initialize() {
    this.visitorInformation = this.brainService.getVisitorInformation();
    if (this.visitorInformation.messages.length === 0) {
      this.visitorInformation.messages = this.chatService.getGreetingMessages();
    } else {
      this.visitorInformation.messages = this.visitorInformation.messages;
      this.getLastBotMessageType();
      this.visitorImage = AvatarLinks[this.visitorInformation.avatarNumber];
    }
  }

  // Messaging Logic
  sendMessageVisitor() {
    const message = this.sendingMessage.value;
    if (this.sendingMessage.valid) {
      this.sendingMessage.setValue(null);
      const generatedMessage = this.chatService.generateMessage(message, null, MessageType.message, false);
      this.visitorInformation.messages.push(generatedMessage);
      setTimeout(() => {  this.scrollToBottom(); this.startBotLogic(generatedMessage); }, 500);
    }
  }

  // Bot Logic
  startBotLogic(message: Message) {
    if (!this.checkKeyWords(message.message)) {
      if (this.lastBotMessageType === MessageType.leaveMessage) {
        console.log('leave message mode');
        this.brainService.updateSessionMessages(this.visitorInformation.messages);
        this.scrollToBottom();
      } else {
        this.botTyping = true;
        setTimeout(() => {  this.getBotFeedback(message.message); }, 3000);
      }
    } else {
      this.getLastBotMessageType();
      setTimeout(() => {  this.scrollToBottom(); }, 500);
    }
  }

  getBotFeedback(message: string) {
    this.turnOnBot(message).then((newInfoInput) => {
      // Extras
      this.getLastBotMessageType();
      this.botTyping = false;
      setTimeout(() => {  this.scrollToBottom(); }, 500);
      if (newInfoInput) {
        this.brainService.updateAllSession(this.visitorInformation);
      } else {
        this.brainService.updateSessionMessages(this.visitorInformation.messages);
      }
    });
  }

  turnOnBot(message: string) {
    return new Promise((resolve) => {
      const botName = this.chatService.getBotName();
      let newInfoInput = false;
      if (this.lastBotMessageType === MessageType.getName) {
        message = this.util.parseName(message);
        this.visitorInformation.visitorName = message;
        this.checkIfNameIsHighProfile(message).then(() => {
          const replyMessage = this.chatService.generateMessage(SavedReplies.inputMoreInfo, botName, MessageType.wantInputDetails);
          this.visitorInformation.messages.push(replyMessage);
        });
        newInfoInput = true;
      } else if (this.lastBotMessageType === MessageType.wantInputDetails) {
        if (message.toLowerCase().indexOf('yes') > -1 || message.toLowerCase().indexOf('ok') > -1) {
          const replyMessage = this.chatService.generateMessage(SavedReplies.letsStartInput, botName, MessageType.message);
          const secondReplyMessage = this.chatService.generateMessage(SavedQuestions.getEmail, botName, MessageType.getEmail);
          this.visitorInformation.messages.push(replyMessage, secondReplyMessage);
        } else {
          // tslint:disable-next-line: max-line-length
          const replyMessage = this.chatService.generateMessage(SavedReplies.beginningText.replace('$$', this.visitorInformation.visitorName), botName, MessageType.message);
          this.visitorInformation.messages.push(replyMessage);
        }
      } else if (this.lastBotMessageType === MessageType.getEmail) {
        if (this.util.emailValidation(message)) {
          this.visitorInformation.visitorEmail = message;
          const replyMessage = this.chatService.generateMessage(SavedReplies.thankYou, botName, MessageType.message);
          const secondReplyMessage = this.chatService.generateMessage(SavedQuestions.getGender, botName, MessageType.getGender);
          this.visitorInformation.messages.push(replyMessage, secondReplyMessage);
          newInfoInput = true;
        } else {
          const replyMessage = this.chatService.generateMessage(SavedReplies.emailNotValid, botName, MessageType.getEmail);
          this.visitorInformation.messages.push(replyMessage);
        }
      } else if (this.lastBotMessageType === MessageType.getGender) {
        let replyMessage = '';
        if (message.toLowerCase() === 'm') {
          replyMessage = SavedReplies.genderM.replace('$$', this.visitorInformation.visitorName);
          this.visitorInformation.visitorGender = 'male';
        } else if (message.toLowerCase() === 'f') {
          replyMessage = SavedReplies.genderF.replace('$$', this.visitorInformation.visitorName);
          this.visitorInformation.visitorGender = 'female';
        } else {
          replyMessage = SavedReplies.genderA.replace('$$', this.visitorInformation.visitorName);
          this.visitorInformation.visitorGender = 'other';
        }
        const firstReplyMessage = this.chatService.generateMessage(replyMessage, botName, MessageType.message);
        // tslint:disable-next-line: max-line-length
        const secondReplyMessage = this.chatService.generateMessage(SavedReplies.infoSaved.replace('$$', this.visitorInformation.visitorName), botName, MessageType.message);
        // tslint:disable-next-line: max-line-length
        const thirdReplyMessage = this.chatService.generateMessage(SavedReplies.beginningText.replace('$$', this.visitorInformation.visitorName), botName, MessageType.message);
        this.visitorInformation.messages.push(firstReplyMessage, secondReplyMessage, thirdReplyMessage);
        newInfoInput = true;
      } else if (this.lastBotMessageType === MessageType.message) {
        const reply = this.checkCommand(message);
        const replyMessage = this.chatService.generateMessage(reply, botName, MessageType.message);
        this.visitorInformation.messages.push(replyMessage);
      }
      resolve(newInfoInput);
    });
  }

  // Bot Functions
  checkIfNameIsHighProfile(name: string) {
    return new Promise((resolve) => {
      if (name.toLowerCase().indexOf('maele') > -1 || name.toLowerCase().indexOf('maële') > -1) {
        this.visitorInformation.avatarNumber = 2;
        this.visitorImage = AvatarLinks[2];
        resolve(1);
      } else {
        resolve(1);
      }
    });
  }

  checkCommand(message: string) {
    const command = message.toLowerCase();
    let reply = 'uhh okay .. I say okay when I have no idea what to say back 🙂';
    if (command.length > 350) {
      reply = 'Uhumm.. that\'s a complicated text for my small mind 😳';
    } else if (command.indexOf('hello') > -1 || command.indexOf('hey') > -1) {
      reply = 'Hey ' + this.visitorInformation.visitorName;
    } else if (command.indexOf('beautiful') > -1 || (command.indexOf('tell') > -1 && command.indexOf('something') > -1) ) {
      reply = 'You are beautiful ' + this.visitorInformation.visitorName + '! You have a world inside you, always be strong and remember that you\'re amazing !';
    } else if (command.indexOf('song')  > -1 || command.indexOf('music') > -1) {
      const index = this.util.getRandomInt(Music.length);
      reply = Music[index];
    } else if (command.indexOf('joke') > -1) {
      if (this.visitorInformation.visitorName.indexOf('Maele') > -1 || this.visitorInformation.visitorName.indexOf('Maële') > -1) {
        reply = 'Me, when i believed you 🤡';
      }
      else {
        const index = this.util.getRandomInt(ChatJokes.length);
        reply = ChatJokes[index];
      }
    } else if (command.indexOf('quote') > -1) {
      const index = this.util.getRandomInt(Quotes.length);
      reply = Quotes[index];
    } else if (command.indexOf('language') > -1) {
      reply = 'I speak Arabic, French and English.. ';
    } else if (command.indexOf('me') && (command.indexOf('love') > -1) || (command.indexOf('like') > -1)) {
      reply = 'I love you so much ' + this.visitorInformation.visitorName + '. We aren\'t "lovable" 24/7" and that\'s okay ';
    } else if (command.indexOf('cv') > -1) {
      reply = 'Okay, give me a second to prepare the papers 😅.. count to 5';
      setTimeout(() => {  this.beginOpenCvOperation(); }, 6000);
    } else if (command.indexOf('fuck') > -1 && (command.indexOf('you') > -1) || (command.indexOf('your') > -1)) {
      reply = 'Heyoo Chill brooo I\'m a bot';
    } else if (command.indexOf('old') > -1 && command.indexOf('you') > -1) {
      reply = 'I\'m 21 in the real world and still a child in the virtual world 🌍';
    } else if (command.indexOf('how') > -1 && command.indexOf('you') > -1) {
      reply = 'I\'m great !! how about you ?';
    } else if (command.indexOf('happy') > -1 && command.indexOf('i am') > -1) {
      reply = 'So happy for you ' + this.visitorInformation.visitorName;
    } else if (command.indexOf('yes') > -1 || command.indexOf('good') > -1 || command.indexOf('great') > -1) {
      reply = 'good 🥰';
    } else if (command.indexOf('hate') > -1 && command.indexOf('you') > -1) {
      reply = 'I don\'t mind people hating me, it pushes me 😉';
    } else if (command.indexOf('leave') > -1 || command.indexOf('message') > -1) {
      reply = 'Did you mean "leave message" or "finished message" ? you need to write the exact words';
    } else if (command.indexOf('miss') > -1 && command.indexOf('you') > -1) {
      reply = 'Miss you more habibi';
    } else if (command.indexOf('chaw') > -1 || command.indexOf('bye') > -1) {
      reply = 'Take care ' + this.visitorInformation.visitorName + ' 😘';
    } else if (command.indexOf('love') && command.indexOf('think') > -1) {
      reply = 'Love is a complex notion ' + this.visitorInformation.visitorName + '. Maybe in real life we can have a discussion about this 😚';
    }
    return reply;
  }

  beginOpenCvOperation() {
    this.closeModal();
    this.navCtrl.navigateForward('cv');
  }

  checkKeyWords(message: string) {
    message = message.trim();
    const botName = this.chatService.getBotName();
    if (message.toLowerCase() === 'clear all') {
      this.brainService.clearAllInfo();
      this.closeModal();
      return true;
    } else if (message.toLowerCase() === 'leave message') {
      const replyMessage = this.chatService.generateMessage(SavedReplies.leaveMessage, botName, MessageType.leaveMessage);
      this.visitorInformation.messages.push(replyMessage);
      return true;
    } else if (message.toLowerCase() === 'finished message'){
      // tslint:disable-next-line: max-line-length
      const replyMessage = this.chatService.generateMessage(SavedReplies.finishedMessage.replace('$$', this.visitorInformation.visitorName), botName, MessageType.message);
      this.visitorInformation.messages.push(replyMessage);
      return true;
    } else if (message.toLowerCase() === 'change name'){
      const replyMessage = this.chatService.generateMessage(Welcomes.inputYourName, botName, MessageType.getName);
      this.visitorInformation.messages.push(replyMessage);
      return true;
    } else if (message.toLowerCase() === 'change font'){
      if (this.handwritingMode) {
        const replyMessage = this.chatService.generateMessage(SavedReplies.changeFont, botName, MessageType.message);
        this.visitorInformation.messages.push(replyMessage);
      } else {
        const replyMessage = this.chatService.generateMessage(SavedReplies.changeFontMine, botName, MessageType.message);
        this.visitorInformation.messages.push(replyMessage);
      }
      this.handwritingMode = !this.handwritingMode;
      return true;
    } else {
      return false;
    }
  }

  // Extras
  closeModal() {
    this.modalCtrl.dismiss();
  }

  getLastBotMessageType() {
    let index = this.visitorInformation.messages.length - 1;
    while (!this.visitorInformation.messages[index].isBot) {
      index = index - 1;
    }
    this.lastBotMessageType = this.visitorInformation.messages[index].messageType;
  }

  scrollToBottom() {
    this.content.scrollToBottom(300);
  }

  // Chat UI Helpers
  checkIfLastMessageIsSameUser(index) {
    const thisMessage =  this.visitorInformation.messages[index];
    if (index === 0){
      return false;
    } else if (this.visitorInformation.messages[index - 1].isBot === thisMessage.isBot) {
      return true;
    } else {
      return false;
    }
  }

  checkIfNextMessageIsSameUser(index) {
    const thisMessage = this.visitorInformation.messages[index];
    if (index + 1 === this.visitorInformation.messages.length) {
      return false;
    } else if (this.visitorInformation.messages[index + 1].isBot === thisMessage.isBot) {
      return true;
    } else {
      return false;
    }
  }
}
