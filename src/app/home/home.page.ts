import { Component, OnInit } from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import {ArtificialJadPage} from '../artificial-jad/artificial-jad.page';
import {UtilService} from "../services/util.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  currentSelection: string = null;
  showChatText = true;

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private util: UtilService
  ) {
  }

  ngOnInit() {
    setTimeout(() => {this.initializeChatHello();}, 2000);
  }

  expandItem(itemName: string) {
    if (this.currentSelection === itemName) {
      this.currentSelection = null;
    } else {
      this.currentSelection = itemName;
    }
  }

  goToCv() {
    this.navCtrl.navigateForward('cv');
  }

  goToWorkface() {
    window.open('https://workface.me');
  }

  goToMyWorkface() {
    window.open('https://workface.me/jadharmoush');
  }

  goToLinkedIn() {
    window.open('https://www.linkedin.com/in/jad-h-60541a114/');
  }

  goToWhatsApp() {
    window.open('https://wa.me/971585842305');
  }

  goToInstagram() {
    window.open('https://www.instagram.com/jad.harmoush/');
  }

  copyEmail() {
    navigator.clipboard.writeText('contact@jadharmoush.com').then(() => {
      this.util.showSwalToast('Email copied to clipboard');
    }).catch(e => console.error(e));
  }

  openSourceCode() {
    window.open('https://gitlab.com/jadharmoush/portfolio-website');
  }

  async openChat() {
    const modal = await this.modalCtrl.create({
      component: ArtificialJadPage,
    });
    return await modal.present();
  }

  private initializeChatHello() {
    setTimeout(() => {this.showChatText = false;}, 7000);
  }
}
