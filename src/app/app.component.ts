import { Component } from '@angular/core';
import {BrainService} from './services/brain.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private brainService: BrainService
  ) {
    this.brainService.initialize();
  }
}
